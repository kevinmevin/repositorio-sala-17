/**
 * \file: timer_interface.h
 *
 */

/* 
 * Created on Sat Apr 23 2021
 *
 * Copyright (c) 2021 Turma P1 da Sala 17 de SETR
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once
#include <stdint.h>
#include <stdbool.h> 


/*
 *  Functions
 */
 

/** \brief Initialize the timer
* @param [in] period The desired period (in ms)
* @return returns a flag indicating if it occurred an error or not (-1 means it occurred an error, 0 means it didn't)
*/
int8_t InitTimer(uint32_t period);


/** \brief Starts the timer by using interrupts
* @param [in] f is the callback function
* @return returns a flag indicating if it occurred an error or not (-1 means it occurred an error, 0 means it didn't)
*/
int8_t StartTimerInterrupt(void *f);


/** \brief Starts the timer by using the pooling method
* @param [in] Flag Pointer to the boolean flag 
* @return returns a flag indicating if it occurred an error or not (-1 means it occurred an error, 0 means it didn't)
*/
int8_t StartTimerPooling(bool *flag);


/** \brief Resets the timer
* @return returns a flag indicating if it occurred an error or not (-1 means it occurred an error, 0 means it didn't)
*/
int8_t ResetTimer(void);


/** \brief Stop timer from running
* @return returns a flag indicating if it occurred an error or not (-1 means it occurred an error, 0 means it didn't)
*/
int8_t StopTimer(void);


/* *****************************************************************************
 End of File
 */
